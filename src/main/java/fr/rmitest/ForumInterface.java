package fr.rmitest;

import fr.rmitest.Model.Question;
import fr.rmitest.Model.User;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ForumInterface extends Remote {
    public User register(String username, String password) throws RemoteException;

    public void disconnect(User user) throws RemoteException;

    public List<Question> listQuestions() throws RemoteException;
}
